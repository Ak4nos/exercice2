package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;

public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer <byte[]>{
	/*
	 * Méthode pour désérialiser une image
	 */
		
		@Override
		public void deserialize(byte[] media) throws IOException {
	        byte[] decodedBytes = Base64.getDecoder().decode(media);
		}

		@Override
		public <K extends InputStream> K getDeserializingStream(byte[] media) throws IOException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T extends OutputStream> T getSourceOutputStream() {
			// TODO Auto-generated method stub
			return null;
		}		
	
}
