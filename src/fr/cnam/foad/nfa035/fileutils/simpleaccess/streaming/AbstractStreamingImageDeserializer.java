package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer <M> {
	
	public final void deserialize(M media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}
